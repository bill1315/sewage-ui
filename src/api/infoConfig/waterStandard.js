// waterStandard 水质标准
import request from '@/utils/request'

// // 查询登录日志列表
// export function list(query) {
//   return request({
//     url: '/monitor/logininfor/list',
//     method: 'get',
//     params: query
//   })
// }
// // 新增部门
// export function addDept(data) {
//   return request({
//     url: '/system/dept',
//     method: 'post',
//     data: data
//   })
// }
// 保存水质标准
export function addSysStandard(data) {
  return request({
    url: '/sys/addSysStandard',
    method: 'post',
    data: data
  })
}
// 全部水质标准列表
export function getSysStandardList(query) {
  return request({
    url: '/sys/getSysStandardList',
    method: 'get',
    params: query
  })
}

// 删除水质标准列表
export function delSysStandard(query) {
  return request({
    url: '/sys/delSysStandard',
    method: 'get',
    params: query
  })
}
//检验名称
export function checkSysName(query) {
  return request({
    url: '/sys/checkSysName',
    method: 'get',
    params: query
  })
}