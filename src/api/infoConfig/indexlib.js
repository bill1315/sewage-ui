// 指标库管理
import request from '@/utils/request'

// 添加标准
export function addIndexlib(data) {
  return request({
    url: '/system/indexlib',
    method: 'post',
    data: data
  })
}

// 修改部门
export function editIndexlib(data) {
  return request({
    url: '/system/indexlib',
    method: 'put',
    data: data
  })
}

// 删除指标
export function delDept(deptId) {
  return request({
    url: '/system/dept/' + deptId,
    method: 'delete'
  })
}

// 指标列表
export function getIndexlibList(query) {
  return request({
    url: '/system/indexlib/list',
    method: 'get',
    params: query
  })
}

// ID查指标
export function getIndexlibListById(id) {
  return request({
    url: '/system/indexlib/list' + id,
    method: 'get'
  })
}