//水厂管理
import request from '@/utils/request'
//查询水厂
export function getWorks(query) {
  return request({
    url: '/sys/getWorks',
    method: 'get',
    params: query
  })
}
//删除水厂
export function delWorks(query) {
  return request({
    url: '/sys/delWorks',
    method: 'get',
    params: query
  })
}
//校验水厂编码
export function checkSysWorks(query) {
  return request({
    url: '/sys/checkSysWorks',
    method: 'get',
    params: query
  })
}
//添加水厂
export function addsysWork(data) {
  return request({
    url: '/sys/addsysWorks',
    method: 'post',
    data: data
  })
}
//获取用户
export function getUserList() {
  return request({
    url: '/system/user/list',
    method: 'get'
  })
}